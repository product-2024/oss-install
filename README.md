# Offline opensslインストール

## インストール

```bash
yum --downloadonly --releasever=7 --installroot=./oss-repo --downloaddir=./oss-repo install -y openssl openssl-devel
yum install -y createrepo
createrepo --database ./oss-repo
```

## インストール後確認

```bash
root@oraclelinux7:/home/vagrant/data/oss-install# rpm -qa | grep openssl
openssl-devel-1.0.2k-26.el7_9.x86_64
openssl-1.0.2k-26.el7_9.x86_64
openssl-libs-1.0.2k-26.el7_9.x86_64
```
