#!/bin/sh
SCRIPT_DIR=$(cd $(dirname -- $0) && pwd)
REPO_TAR_FILE=oss-repo.tar.gz

cd ${SCRIPT_DIR}
tar xfvz ${REPO_TAR_FILE}

# リポジトリバックアップ
mkdir -p /etc/yum.repos.d/bak
mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/bak/.

# プライベートリポジトリ配備
REPO_DIR=`basename $REPO_TAR_FILE .tar.gz`
cat ${SCRIPT_DIR}/Private.repo | sed -e "s|SCRIPT_DIR|$SCRIPT_DIR|g" | sed -e "s|OSS_REPO|$REPO_DIR|g" > /etc/yum.repos.d/Private.repo

# Ansibleインストール
echo "yum info ansible"
yum info ansible
echo ""

echo "yum install -y openssl openssl-devel"
yum install -y openssl openssl-devel

rm -f /etc/yum.repos.d/Private.repo
mv /etc/yum.repos.d/bak/*.repo /etc/yum.repos.d/.
rm -rf /etc/yum.repos.d/bak

rm -rf ${SCRIPT_DIR}/${REPO_DIR}
